# Правило для указания всех выходных артефактов
rule all:
    input:
        "data_snake/preprocess_data_drop.csv",
        "data_snake/preprocess_data_fill.csv",
        "models/model_drop_rf.pkl",
        "models/model_fill_rf.pkl",
        "models/model_drop_lr.pkl",
        "models/model_fill_lr.pkl"

# Правило для чтения данных
rule read_data:
    output:
        "data_snake/tree_data.csv"
    script:
        "scripts/read_data.py"

# Правило для препроцессинга данных
# Удаление пропущенных значений
rule preprocess_drop:
    input:
        "data_snake/tree_data.csv"
    output:
        "data_snake/preprocess_data_drop.csv"
    threads: 4
    script:
        "scripts/preprocess_drop.py"

# Правило для препроцессинга данных
# Замена пропущенных значений на ноль
rule preprocess_fill:
    input:
        "data_snake/tree_data.csv"
    output:
        "data_snake/preprocess_data_fill.csv"
    threads: 4
    script:
        "scripts/preprocess_fill.py"

# Правило обучения модели логистической регрессии
# Датасет с заменой пропущенных значений на ноль
rule model_fill_lr:
    input:
        "data_snake/preprocess_data_fill.csv"
    output:
        "models/model_fill_lr.pkl"
    script:
        "scripts/train_model_lr.py"

# Правило обучения модели логистической регрессии
# Датасет с удалением пропущенных значений
rule model_drop_lr:
    input:
        "data_snake/preprocess_data_drop.csv"
    output:
        "models/model_drop_lr.pkl"
    script:
        "scripts/train_model_lr.py"

# Правило обучения модели случайный лес
# Датасет с заменой пропущенных значений на ноль
rule model_fill_rf:
    input:
        "data_snake/preprocess_data_fill.csv"
    output:
        "models/model_fill_rf.pkl"
    script:
        "scripts/train_model_rf.py"

# Правило обучения модели случайный лес
# Датасет с удалением пропущенных значений
rule model_drop_rf:
    input:
        "data_snake/preprocess_data_drop.csv"
    output:
        "models/model_drop_rf.pkl"
    script:
        "scripts/train_model_rf.py"