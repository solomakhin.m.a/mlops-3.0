"""Module model learning."""

import numpy as np
from catboost import CatBoostClassifier  # type: ignore
from clearml import Task
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix

task_model_train_lr = Task.create(
    project_name="MLOps train",
    task_name="Model train lr",
    task_type=Task.TaskTypes.training,
)


def train_logistic_regression(  # noqa: D103
    data: np.ndarray, target: np.ndarray, model_params: dict
) -> LogisticRegression:
    model_lr = LogisticRegression(**model_params)
    model_lr.fit(data, target)
    return model_lr


task_model_train_cb = Task.create(
    project_name="MLOps train",
    task_name="Model train cb",
    task_type=Task.TaskTypes.training,
)


def train_catboost(  # noqa: D103
    data: np.ndarray, target: np.ndarray, model_params: dict
) -> CatBoostClassifier:
    model_cb = CatBoostClassifier(**model_params)
    model_cb.fit(data, target)
    return model_cb


task_model_test_lr = Task.create(
    project_name="MLOps train",
    task_name="Model test lr",
    task_type=Task.TaskTypes.training,
)


def test_logistic_regression(  # noqa: D103
    model: LogisticRegression, data: np.ndarray, target: np.ndarray
) -> tuple[dict, np.ndarray]:
    predicts = model.predict(data)
    return classification_report(target, predicts, output_dict=True), confusion_matrix(
        target, predicts
    )


task_model_test_cb = Task.create(
    project_name="MLOps train",
    task_name="Model test cb",
    task_type=Task.TaskTypes.training,
)


def test_catboost(  # noqa: D103
    model: CatBoostClassifier, data: np.ndarray, target: np.ndarray
) -> tuple[dict, np.ndarray]:
    predicts = model.predict(data)
    return classification_report(target, predicts, output_dict=True), confusion_matrix(
        target, predicts
    )
