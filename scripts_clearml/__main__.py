"""Module import function."""

from scripts_clearml import cli, model, preprocessing, vectorize  # noqa: F401

if __name__ == "__main__":
    cli.cli_clearml()
