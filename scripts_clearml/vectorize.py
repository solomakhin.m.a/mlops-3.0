"""Module vectorize."""

import polars as pl
from clearml import Task
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split

task_vectorize = Task.create(
    project_name="MLOps train",
    task_name="Vectorize",
    task_type=Task.TaskTypes.data_processing,
)


def train_vectorize(  # noqa: D103
    data: pl.DataFrame, vectorizer_params: dict, random_state: int
) -> tuple[TfidfVectorizer, pl.DataFrame, pl.DataFrame]:
    tfidf_vectorizer = TfidfVectorizer(**vectorizer_params)

    train, val = train_test_split(
        data,
        test_size=0.3,
        shuffle=True,
        random_state=random_state,
    )
    tfidf_vectorizer.fit(train["corpus"].list.join(" ").to_numpy())
    return tfidf_vectorizer, train, val


def apply_vectorizer(  # noqa: D103
    vectorizer: TfidfVectorizer, data: pl.DataFrame
) -> pl.DataFrame:  # noqa: D103
    return vectorizer.transform(data["corpus"].list.join(" ").to_numpy())
