"""Pipeline clearml."""

from clearml.automation import PipelineController

pipe = PipelineController(
    name="Pipeline clearml",
    project="MLOps train",
    version="0.0.1",
    add_pipeline_tags=False,
)

pipe.set_default_execution_queue("default")

pipe.add_step(
    name='cli_clearml_step',
    base_task_project="MLOps train",
    base_task_name="CLI ClearML",
    cache_executed_step=True,
)

pipe.add_step(
    name='preprocessing_step',
    base_task_project="MLOps train",
    base_task_name="Preprocessing",
    parents=['cli_clearml_step'],
    cache_executed_step=True,
)

pipe.add_step(
    name='vectorize_step',
    base_task_project="MLOps train",
    base_task_name="Vectorize",
    parents=['preprocessing_step'],
    cache_executed_step=True,
)

pipe.add_step(
    name='model_train_lr',
    base_task_project="MLOps train",
    base_task_name="Model train lr",
    parents=['vectorize_step'],
)

pipe.add_step(
    name='model_train_cb',
    base_task_project="MLOps train",
    base_task_name="Model train cb",
    parents=['vectorize_step'],
)

pipe.add_step(
    name='model_test_lr',
    base_task_project="MLOps train",
    base_task_name="Model test lr",
    parents=['model_train_lr'],
)

pipe.add_step(
    name='model_test_cb',
    base_task_project="MLOps train",
    base_task_name="Model test cb",
    parents=['model_train_cb'],
)

pipe.start_locally()

print('Done')
