"""Module cli."""

import click
import joblib
import polars as pl
from clearml import Dataset, Task

from scripts_clearml.model import (
    test_catboost,
    test_logistic_regression,
    train_catboost,
    train_logistic_regression,
)
from scripts_clearml.preprocessing import dataframe_preprocessing
from scripts_clearml.vectorize import apply_vectorizer, train_vectorize


@click.command()
@click.argument("vec_max_feature", type=int, default=1000)
@click.argument("vec_analyzer", type=str, default="word")
@click.argument("random_state", type=int, default=42)
@click.argument("lr_multi_class", type=str, default="multinomial")
@click.argument("lr_solver", type=str, default="saga")
@click.argument("cb_iterations", type=int, default=100)
@click.argument("cb_learning_rate", type=float, default=0.1)
@click.argument("cb_depth", type=int, default=6)
def cli_clearml(  # noqa: D103
    vec_max_feature: int,
    vec_analyzer: str,
    random_state: int,
    lr_multi_class: str,
    lr_solver: str,
    cb_iterations: int,
    cb_learning_rate: float,
    cb_depth: int,
):
    task_cli_clearml = Task.init(
        project_name="MLOps train", task_name="CLI ClearML", output_uri=True
    )

    frame_path = Dataset.get(
        dataset_name="Raw data", dataset_project="Amazon reviews"
    ).get_local_copy()
    task_cli_clearml.set_progress(0)
    data = pl.read_csv(
        frame_path + "/amazon_train.csv",
        has_header=False,
        new_columns=["Polarity", "Title", "Review"],
        n_rows=5000,
    )
    task_cli_clearml.set_progress(10)

    processed_data = dataframe_preprocessing(data, "Review")
    task_cli_clearml.set_progress(20)
    task_cli_clearml.upload_artifact(
        name="processed_data", artifact_object=processed_data
    )

    vectorizer, train_data, test_data = train_vectorize(
        processed_data,
        {"max_features": vec_max_feature, "analyzer": vec_analyzer},
        random_state,
    )
    joblib.dump(vectorizer, "models/vectorizer_clearml.pkl", compress=True)

    train_result = apply_vectorizer(vectorizer, train_data)
    test_result = apply_vectorizer(vectorizer, test_data)
    task_cli_clearml.set_progress(50)
    task_cli_clearml.upload_artifact(
        name="train_features",
        artifact_object=(train_result, train_data["Polarity"].to_numpy()),
    )
    task_cli_clearml.upload_artifact(
        name="test_features",
        artifact_object=(test_result, test_data["Polarity"].to_numpy()),
    )
    model_lr = train_logistic_regression(
        train_result,
        train_data["Polarity"].to_numpy(),
        {
            "random_state": random_state,
            "multi_class": lr_multi_class,
            "solver": lr_solver,
        },
    )
    joblib.dump(model_lr, "models/model_lr_clearml.pkl", compress=True)
    task_cli_clearml.set_progress(60)
    result_lr, confusion_lr = test_logistic_regression(
        model_lr, test_result, test_data["Polarity"].to_numpy()
    )
    task_cli_clearml.set_progress(70)
    logger_lr = task_cli_clearml.get_logger()
    logger_lr.report_single_value("accuracy_lr", result_lr.pop("accuracy"))
    for class_name, metrics in result_lr.items():
        for metric, value in metrics.items():
            logger_lr.report_single_value(f"{class_name}_{metric}_lr", value)
    logger_lr.report_confusion_matrix(
        "confusion matrix_lr", "ignored", matrix=confusion_lr
    )

    model_cb = train_catboost(
        train_result,
        train_data["Polarity"].to_numpy(),
        {
            "iterations": cb_iterations,
            "depth": cb_depth,
            "random_state": random_state,
            "learning_rate": cb_learning_rate,
        },
    )
    joblib.dump(model_cb, "models/model_cb_clearml.pkl", compress=True)

    task_cli_clearml.set_progress(80)
    result_cb, confusion_cb = test_catboost(
        model_cb, test_result, test_data["Polarity"].to_numpy()
    )
    task_cli_clearml.set_progress(90)
    logger_cb = task_cli_clearml.get_logger()
    logger_cb.report_single_value("accuracy_cb", result_cb.pop("accuracy"))
    for class_name, metrics in result_cb.items():
        for metric, value in metrics.items():
            logger_cb.report_single_value(f"{class_name}_{metric}_cb", value)
    logger_cb.report_confusion_matrix(
        "confusion matrix_cb", "ignored", matrix=confusion_cb
    )
