"""Module preprocessing."""

import re

import nltk
import polars as pl
from clearml import Task
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

nltk.download("stopwords")

task_preprocessing = Task.create(
    project_name="MLOps train",
    task_name="Preprocessing",
    task_type=Task.TaskTypes.data_processing,
)


def text_preprocessing(input_text: str) -> str:  # noqa: D103
    text = input_text.lower()  # приведение к нижнему регистру
    text = re.sub(
        r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*", "", text
    )  # убираем ссылки
    text = re.sub("[0-9 \-_]+", " ", text)  # убираем спец символы
    text = re.sub("[^a-z A-Z]+", " ", text)  # оставляем только буквы
    text = " ".join(  # убираем стоп слова
        [word for word in text.split() if word not in stopwords.words("english")]
    )
    return text.strip()


def lemmatize(input_frame: pl.DataFrame) -> pl.DataFrame:  # noqa: D103
    lemmatizer = WordNetLemmatizer()

    return input_frame.with_columns(
        pl.col("corpus").map_elements(
            lambda input_list: [lemmatizer.lemmatize(token) for token in input_list]
        )
    )


def dataframe_preprocessing(  # noqa: D103
    data: pl.DataFrame, col_name: str
) -> pl.DataFrame:  # noqa: D103
    return lemmatize(
        data.with_columns(
            pl.col(col_name)
            .map_elements(text_preprocessing)
            .str.split(" ")
            .alias("corpus")
        )
    )
