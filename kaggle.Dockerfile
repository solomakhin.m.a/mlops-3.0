FROM mambaorg/micromamba

COPY pyproject.toml pdm.lock ./

RUN micromamba create -n myenv python=3.11 jupyter pdm -c conda-forge
RUN eval "$(micromamba shell hook --shell bash)" \
    && micromamba activate myenv \
    && pdm install -G dev \
    && pip install -U quarto \
    && pip install kaggle \
    && kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data 

USER root
RUN mkdir /datasets
RUN apt-get update && apt-get install -y unzip
RUN unzip ny-2015-street-tree-census-tree-data.zip -d /datasets
USER some-non-root-user

USER root

ENTRYPOINT ["micromamba", "run", "-n", "myenv", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--notebook-dir=/datasets", "--allow-root"]