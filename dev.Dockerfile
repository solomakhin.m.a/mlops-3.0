FROM mambaorg/micromamba

WORKDIR /app

COPY pyproject.toml pdm.lock ./

RUN micromamba create -n myenvironment python=3.11 jupyter pdm -c conda-forge -y

RUN eval "$(micromamba shell hook --shell bash)" \
    && micromamba activate myenvironment \
    && pdm install -G dev

ENTRYPOINT [ "micromamba", "run", "-n", "myenvironment", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root" ]
