"""Модуль чтения данных."""

import pandas as pd

data = pd.read_csv("dataset/tree_data.csv")  # type: ignore  # noqa: F821

data.to_csv(snakemake.output[0], index=False)  # type: ignore  # noqa: F821
