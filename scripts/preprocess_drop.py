"""Модуль препроцессинга данных."""

import pandas as pd

data = pd.read_csv(snakemake.input[0])  # type: ignore  # noqa: F821
data['created_at'] = pd.to_datetime(data['created_at']).astype(int)

processed_data = data.dropna()
categorical_columns = processed_data.select_dtypes(
    include=['object', 'category']
).columns  # noqa: E501
processed_data = pd.get_dummies(processed_data, columns=categorical_columns)
processed_data = processed_data[:5000]

processed_data.to_csv(snakemake.output[0], index=False)  # type: ignore  # noqa: F821
