"""Модуль обучения модели."""

import joblib  # type: ignore
import pandas as pd
from sklearn.ensemble import RandomForestRegressor  # type: ignore
from sklearn.model_selection import train_test_split  # type: ignore

data = pd.read_csv(snakemake.input[0])  # type: ignore  # noqa: F821

X = data.drop("tree_dbh", axis=1)  # assuming "target" is the target variable
y = data["tree_dbh"]
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

model = RandomForestRegressor()
model.fit(X_train, y_train)
accuracy = model.score(X_test, y_test)

print(f'Model accuracy: {accuracy}')
joblib.dump(model, snakemake.output[0])  # type: ignore  # noqa: F821
