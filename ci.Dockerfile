FROM mambaorg/micromamba

WORKDIR /app

COPY env.yml /app/env.yml
RUN micromamba create -n myenv -f env.yml -y
RUN micromamba run -n myenv pip install pdm
RUN micromamba run -n myenv pdm use /opt/conda/envs/myenv
COPY pyproject.toml /app/pyproject.toml
COPY pdm.lock /app/pdm.lock
RUN micromamba run -n myenv pdm install -G dev